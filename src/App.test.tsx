import React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';
import App, { AppProps } from './App';
import { findByDataTestAttr } from '../test/test-utils';
import Congrats from './Congrats';

const setup = (props: AppProps = {}): ShallowWrapper =>
  shallow(<App {...props} />);

test('renders without error', () => {
  const wrapper = setup();
  const appComponent = findByDataTestAttr(wrapper, 'app-component');
  expect(appComponent.length).toBe(1);
});

test('does not render Congrats component if `success` variable is FALSE (on default)', () => {
  const wrapper = setup();
  const congratsComponent = findByDataTestAttr(wrapper, 'congrats-component');
  expect(congratsComponent.contains(<Congrats />)).toBe(false);
});

// test('renders Congrats component if `success` variable is TRUE', () => {
//     const wrapper = setup();
//     const appComponent = findByDataTestAttr(wrapper, 'congrats-component');
//     expect(appComponent.length).toBe(1);
// });

// test('renders a button', () => {
//   const wrapper = setup();
//   const button = findByDataTestAttr(wrapper, 'increment-button');
//   expect(button.length).toBe(1);
// });
//
// test('renders counter display', () => {
//   const wrapper = setup();
//   const counterDisplay = findByDataTestAttr(wrapper, 'counter-display');
//   expect(counterDisplay.length).toBe(1);
// })
//
// test('counter starts with at 0', () => {
//   const wrapper = setup();
//   const count = findByDataTestAttr(wrapper, 'count').text();
//   expect(count).toBe("0");
// })
//
// test('clicking on the button increments counter display', () => {
//   const wrapper = setup();
//   const button = findByDataTestAttr(wrapper, 'increment-button');
//   button.simulate('click');
//   const newCount = findByDataTestAttr(wrapper, 'count').text();
//   expect(newCount).toBe("1");
// })
