export enum ActionTypes {
  CORRECT_GUESS = 'CORRECT_GUESS',
}

export const correctGuess = (): any => {
  return {
    type: ActionTypes.CORRECT_GUESS,
  };
};
