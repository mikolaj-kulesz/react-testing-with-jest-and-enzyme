import React, { useState } from 'react';
import './App.css';
import Congrats from './Congrats';
import GuessedWords from './GuessedWords';
import Input from './Input/Input';

export interface AppProps {
  someProp?: string;
}

const App: React.FC<AppProps> = () => {
  const [success] = useState(true);
  return (
    <div data-test="app-component" className="App">
      <h1>Joto Game</h1>
      {success && <Congrats />}
      <Input />
      <GuessedWords
        guessedWords={[
          {
            word: 'someDummyWord1',
            letterMatchCount: 3,
          },
          {
            word: 'someDummyWord2',
            letterMatchCount: 4,
          },
        ]}
      />
    </div>
  );
};

export default App;
