import { ActionTypes } from '../actions';

interface Action {
  type?: ActionTypes;
}

export default (state: string[] | undefined, action: Action): boolean => {
  switch (action.type) {
    case ActionTypes.CORRECT_GUESS:
      return true;
    default:
      return false;
  }
};
