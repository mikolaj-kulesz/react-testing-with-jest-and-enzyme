import React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';
import { findByDataTestAttr, findByText } from '../../test/test-utils';
import Input from './Input';
import GuessedWords, { GuessedWordsProps } from '../GuessedWords/GuessedWords';
import storeFactory from '../test/testUtils';

const setup = (initialState: any = {}): ShallowWrapper => {
  const store = storeFactory(initialState);
  const wrapper = shallow(<Input store={store} />).dive();
  console.log(wrapper.debug());
  return wrapper;
};

describe('render', () => {
  describe('word has not been guessed', () => {
    let wrapper: ShallowWrapper;
    beforeEach(() => {
      const initialState = {};
      wrapper = setup(initialState);
    });

    test('renders component without error', () => {
      const component = findByDataTestAttr(wrapper, 'input-component');
      expect(component.length).toBe(1);
    });

    test('renders input text', () => {});

    test('renders submit button', () => {});
  });

  describe('word has been guessed', () => {
    test('renders component without error', () => {});

    test('does not render input text', () => {});

    test('does not renders submit button', () => {});
  });
});

describe('update state', () => {});
