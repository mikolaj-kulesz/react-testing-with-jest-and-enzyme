import React from 'react';
import { connect } from 'react-redux';

interface InputProps {
  store?: any;
}

const Input: React.FC<InputProps> = () => {
  return <div data-test="input-component">Input</div>;
};

const mapStateToProps = ({ success }: { success: boolean }): any => {
  return {
    success,
  };
};

export default connect()(Input);
