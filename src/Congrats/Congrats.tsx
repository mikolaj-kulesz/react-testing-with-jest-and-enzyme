import React from 'react';
import { Alert, AlertTitle } from '@material-ui/lab';

export interface CongratsProps {
  someProp?: string;
}

const Congrats: React.FC<CongratsProps> = () => {
  return (
    <Alert severity="success" data-test="congrats-component">
      <AlertTitle data-test="congrats-message">Success!</AlertTitle>
      This is a success alert — <strong>check it out!</strong>
    </Alert>
  );
};

export default Congrats;
