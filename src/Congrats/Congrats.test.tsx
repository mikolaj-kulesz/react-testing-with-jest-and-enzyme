import React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';
import Congrats, { CongratsProps } from './Congrats';
import { findByDataTestAttr } from '../../test/test-utils';

const setup = (props: CongratsProps = {}): ShallowWrapper =>
  shallow(<Congrats {...props} />);

test('renders without error', () => {
  const wrapper = setup();
  const component = findByDataTestAttr(wrapper, 'congrats-component');
  expect(component.length).toBe(1);
});

test('renders non-empty congrats message', () => {
  const wrapper = setup();
  const congratsMessage = findByDataTestAttr(wrapper, 'congrats-message');
  expect(congratsMessage.text().length).not.toBe(0);
});

test('renders congrats message', () => {
  const wrapper = setup();
  const congratsMessage = findByDataTestAttr(wrapper, 'congrats-message');
  expect(congratsMessage.text()).toBe('Success!');
});
