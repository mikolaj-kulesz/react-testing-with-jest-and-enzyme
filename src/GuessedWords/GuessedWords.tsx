import React from 'react';
import {
  withStyles,
  Theme,
  createStyles,
  makeStyles,
} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { Typography } from '@material-ui/core';

const StyledTableCell = withStyles((theme: Theme) =>
  createStyles({
    head: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    body: {
      fontSize: 14,
    },
  })
)(TableCell);

const StyledTableRow = withStyles((theme: Theme) =>
  createStyles({
    root: {
      '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
      },
    },
  })
)(TableRow);

const useStyles = makeStyles({
  table: {
    minWidth: 700,
  },
});

export interface GuessedWord {
  word: string;
  letterMatchCount: number;
}

export interface GuessedWordsProps {
  guessedWords: GuessedWord[];
}

const GuessedWords: React.FC<GuessedWordsProps> = ({ guessedWords }) => {
  const classes = useStyles();
  const isGuessedWords = !!guessedWords.length;
  const table = (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="customized table">
        <TableHead>
          <TableRow>
            <StyledTableCell>Word</StyledTableCell>
            <StyledTableCell align="right">Letter Match Count</StyledTableCell>
          </TableRow>
        </TableHead>
        <TableBody data-test="guessed-words-list">
          {guessedWords.map(({ word, letterMatchCount }) => (
            <StyledTableRow key={word} data-test="guessed-words-list-item">
              <StyledTableCell component="th" scope="row">
                {word}
              </StyledTableCell>
              <StyledTableCell align="right">
                {letterMatchCount}
              </StyledTableCell>
            </StyledTableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
  const instructions = (
    <Typography data-test="guessed-words-instructions" variant="body1">
      Try to guess a secret word!
    </Typography>
  );
  return (
    <div data-test="guessed-words-component">
      <Typography variant="h4">Guessed Words:</Typography>
      {isGuessedWords ? table : instructions}
    </div>
  );
};

export default GuessedWords;
