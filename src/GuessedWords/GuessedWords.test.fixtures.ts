import { GuessedWord, GuessedWordsProps } from './GuessedWords';

const getDefaultGuessedWords = (): GuessedWord[] => [
  {
    word: 'someDummyWord1',
    letterMatchCount: 3,
  },
  {
    word: 'someDummyWord2',
    letterMatchCount: 4,
  },
];

export const guessedWordsFixtures = (
  guessedWords: GuessedWord[] = []
): GuessedWordsProps => ({
  guessedWords: [...getDefaultGuessedWords(), ...guessedWords],
});

export default guessedWordsFixtures;
