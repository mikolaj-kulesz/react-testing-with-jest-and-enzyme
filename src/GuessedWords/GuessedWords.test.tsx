import React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';
import GuessedWords, { GuessedWordsProps } from './GuessedWords';
import { findByDataTestAttr, findByText } from '../../test/test-utils';
import { guessedWordsFixtures } from './GuessedWords.test.fixtures';

const setup = (
  props: GuessedWordsProps = { guessedWords: [] }
): ShallowWrapper => shallow(<GuessedWords {...props} />);

describe('if there are no guessed words ', () => {
  let wrapper: ShallowWrapper;
  beforeEach(() => {
    wrapper = setup();
  });

  test('component renders without error', () => {
    const element = findByDataTestAttr(wrapper, 'guessed-words-component');
    expect(element.length).toBe(1);
  });

  test('instructions renders without error', () => {
    const element = findByDataTestAttr(wrapper, 'guessed-words-instructions');
    expect(element.length).toBe(1);
  });

  test('words list is disabled`', () => {
    const element = findByDataTestAttr(wrapper, 'guessed-words-list');
    expect(element.length).toBe(0);
  });
});

describe('if there are guessed words', () => {
  let wrapper: ShallowWrapper;
  let props: GuessedWordsProps;

  beforeEach(() => {
    props = guessedWordsFixtures();
    wrapper = setup(props);
  });

  test('component renders without error', () => {
    const element = findByDataTestAttr(wrapper, 'guessed-words-component');
    expect(element.length).toBe(1);
  });

  test('instructions are disabled`', () => {
    const element = findByDataTestAttr(wrapper, 'guessed-words-instructions');
    expect(element.length).toBe(0);
  });

  test('words list renders without error`', () => {
    const element = findByDataTestAttr(wrapper, 'guessed-words-list');
    expect(element.length).toBe(1);
  });

  test('words list items should equal number of elements in `guessedWords` prop array', () => {
    const element = findByDataTestAttr(wrapper, 'guessed-words-list-item');
    expect(element.length).toBe(2);
  });

  test('one of words list item should match example word', () => {
    const customProps = guessedWordsFixtures([
      {
        word: 'XXXX',
        letterMatchCount: 7,
      },
    ]);
    const customWrapper = setup(customProps);
    const element = findByText(customWrapper, 'XXXX');
    // console.log(customWrapper.debug());
    expect(element.length).toBe(2);
  });
});
