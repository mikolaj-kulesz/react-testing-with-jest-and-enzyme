import { getLetterMatchCount } from './utils';

describe('getLetterMatchCount', () => {
  const secretWord = 'party';
  test('return correct count when no letters are matching', () => {
    const letterCountMatch = getLetterMatchCount('bones', secretWord);
    expect(letterCountMatch).toBe(0);
  });

  test('return correct count when 3 letters are matching', () => {
    const letterCountMatch = getLetterMatchCount('train', secretWord);
    expect(letterCountMatch).toBe(3);
  });

  test('return correct count when there are duplicate letters in guess word', () => {
    const letterCountMatch = getLetterMatchCount('parka', secretWord);
    expect(letterCountMatch).toBe(3);
  });
});
