export const getLetterMatchCount = (
  guessedWord: string,
  secretWord: string
): number => {
  const guessedWordLetterSet = new Set(guessedWord.split(''));
  const secretWordLetterSet = new Set(secretWord.split(''));
  return Array.from(secretWordLetterSet.values()).filter((secretWordLetter) =>
    guessedWordLetterSet.has(secretWordLetter)
  ).length;
};

export default getLetterMatchCount;
