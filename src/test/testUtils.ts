import { createStore } from 'redux';
import rootReducer from '../reducers';

export const storeFactory = (initialStore: any) => {
  return createStore(rootReducer, initialStore);
};

export default storeFactory;
