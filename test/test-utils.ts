import { ShallowWrapper } from 'enzyme';

export const findByDataTestAttr = (
  wrapper: ShallowWrapper,
  attributeName: string
) => wrapper.find(`[data-test="${attributeName}"]`);

export const findByText = (wrapper: ShallowWrapper, text: string) => {
  // TODO: check this function
  return wrapper.findWhere((x) => x.text() === text);
};
